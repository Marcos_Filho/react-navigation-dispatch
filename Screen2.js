import React from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';
import { CommonActions } from '@react-navigation/native';

const Screen2 = (props) => {

    const handlePress = () => {        
        const { navigation } = props
        navigation.dispatch(
            CommonActions.navigate({
              name: 'Screen1',
              params: {param: "Param"},
            })
          );
    }

    return (
        <View style={styles.container}>
            <Text>Screen2</Text>
            <Button title='navigate to Screen1' onPress={handlePress} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

export default Screen2;