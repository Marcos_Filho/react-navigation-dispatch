import React, { useEffect, useState } from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';

const Screen1 = (props) => {

    const [hasNavigate, setNavigate] = useState("Screen 1");

    const updateData = (data) => {
        // do what you want with the data  
        if (data)
            setNavigate(data.param)
    }

    const handlePress = () => {
        props.navigation.navigate('Screen2')
    }

    useEffect(() => {
        updateData(props.route.params);
    });

    return (
        <View style={styles.container}>
            <Text>{hasNavigate}</Text>
            <Button title='navigate to Screen2' onPress={handlePress} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

export default Screen1;
