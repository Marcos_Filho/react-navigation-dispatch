import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack'

import screen1 from './Screen1'
import screen2 from './Screen2'

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Screen1" component={screen1} options={{ headerShown: false }} />
        <Stack.Screen name="Screen2" component={screen2} options={{ headerShown: false }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

